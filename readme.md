Prueba buscador - Chappsolutions

Para ejecutar el c�digo se deben instalar las dependencias v�a composer, npm y yarn

Posteriormente se debe lanzar el "yarn encore dev"

La base de datos sqlite se encuentra en var/data.db

Lanzando la orden "php bin/console doctrine:fixtures:load" podremos recargar la base de datos. La cual contiene un usuario administrador (username: admin, pass: secret). Y los 4 tipos de habitaciones de hotel.
