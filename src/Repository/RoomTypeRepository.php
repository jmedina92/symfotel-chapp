<?php

namespace App\Repository;

use App\Entity\RoomType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RoomType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomType[]    findAll()
 * @method RoomType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RoomType::class);
    }



    /**
     * @param $price
     * @return RoomType[]
     */
    public function findByMaxCapacityAndAvailability($persons, $from_date, $to_date): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.max_capacity >= :persons')
            ->andWhere('p.availability_from <= :from_date')
            ->andWhere('p.availability_to >= :to_date')
            ->setParameter('persons', $persons)
            ->setParameter('from_date', $from_date)
            ->setParameter('to_date', $to_date)
            ->getQuery()
            ->getResult();
    }



    // /**
    //  * @return RoomType[] Returns an array of RoomType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoomType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
