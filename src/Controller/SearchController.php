<?php
namespace App\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\RoomType;
use App\Entity\Booking;

class SearchController extends AbstractController
{
    /**
    * @Route("/search", name="search")
    */
    public function search(Request $request)
    {
        //Parámetros obtenidos de la petición
        $from_date = new \DateTime($request->query->get('from_date'));
        $to_date = new \DateTime($request->query->get('to_date'));
        $num_persons = $request->query->get('num_persons');

        //Calculamos el número de días
        $num_days = date_diff($from_date,$to_date)->days + 1;

        //Comprobamos que las fechas son correctas
        if($from_date <= $to_date){

            //Creamos una colección a partir de los tipos de habitaciones que pueden recibir el número de personas solicitados
            $room_types = new ArrayCollection($this->getDoctrine()->getRepository(RoomType::class)->findByMaxCapacityAndAvailability($num_persons, $from_date, $to_date));

            //Filtramos las habitaciones que tienen disponibilidad para las fechas seleccionadas
            $room_types_available = $room_types->filter(function(RoomType $room_type) use ($from_date, $to_date){
                return $room_type->getAvailabilityBetweenDates($from_date, $to_date);
            });
            //Devolvemos las habitaciones filtradas junto con los parámetros de la búsqueda
            return $this->render('search_results.html.twig', [  'rooms' => $room_types_available, 
                                                                'num_days' => $num_days,
                                                                'from_date' => $from_date,    
                                                                'to_date' => $to_date,
                                                                'num_persons' => $num_persons
                                                            ]);
        }
        //En caso de haber algún error en las fechas
        $this->addFlash(
            'error',
            'Error intentar reservar. Revise las fechas seleccionadas.'
        );
        return $this->redirect('/');

    }
    
}