<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\User;
use App\Entity\Booking;
use App\Entity\RoomType;

class HomeController extends AbstractController
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    /**
    * @Route("")
    */
    public function index()
    {
        return $this->render('home.html.twig');
    }

    /**
    * @Route("/dashboard", name="dashboard")
    * @IsGranted("IS_AUTHENTICATED_FULLY")
    * Método para acceder al dashboard de la aplicación.
    * Cambia en función del rol del usuario logueado
    */
    public function dashboard()
    {
        if($this->isGranted('ROLE_ADMIN')){
            $bookings = $this->getDoctrine()->getRepository(Booking::class)->findAll();
            $room_types = $this->getDoctrine()->getRepository(RoomType::class)->findAll();
            return $this->render('dashboards/admin_dashboard.html.twig', compact('bookings', 'room_types'));
        }
        return $this->render('dashboards/dashboard.html.twig');
    }

    /**
    * @Route("/register", name="register", methods={"POST"})
    * Método que registra un nuevo usuario
    */
    public function register(Request $request)
    {
        $user = new User();
        $user->setUsername($request->request->get('username'));
        $user->setEmail($request->request->get('email'));
        $user->setName($request->request->get('name'));
        $user->setSurname($request->request->get('surname'));
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $request->request->get('password')
        ));

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();
        return $this->render('security/register_success.html.twig');
    }
}