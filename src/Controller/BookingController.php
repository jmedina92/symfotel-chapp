<?php
namespace App\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\RoomType;
use App\Entity\Booking;

class BookingController extends AbstractController
{
    /**
    * @Route("/booking", name="booking", methods={"POST"})
    * Método que crea una reserva
    */
    public function booking(Request $request)
    {
        //Parámetros obtenidos de la petición
        $from_date = new \DateTime($request->request->get('from_date'));
        $to_date = new \DateTime($request->request->get('to_date'));
        $num_persons = $request->request->get('num_persons');

        //Obtenemos el tipo de habitación para realizar algunas comprobaciones previas
        $room_type = $this->getDoctrine()->getRepository(RoomType::class)->find($request->request->get('room_type_id'));
        
        //Comprobamos que las fechas siguen siendo correctas, que hay disponibilidad y que el número de personas encaja en la habitación
        if($from_date <= $to_date && $room_type->getAvailabilityBetweenDates($from_date, $to_date) && $room_type->getMaxCapacity() >= $num_persons){
            //Número de días de la reserva
            $num_days = date_diff($from_date,$to_date)->days + 1;
            //Localizador único
            $locator = uniqid('SYMF'); 
            //Creamos la reserva
            $booking = new Booking();
            $booking->setEmail($request->request->get('email'));
            $booking->setName($request->request->get('name'));
            $booking->setSurname($request->request->get('surname'));
            $booking->setFromDate($from_date);
            $booking->setToDate($to_date);
            $booking->setLocator($locator);
            $booking->setNumPeople($num_persons);
            //Calculamos el precio de la reserva y lo guardamos
            $booking->setTotalCost($room_type->getPriceForTotalNights($num_days));
            $booking->setPhone($request->request->get('phone'));
            $booking->setCredictCardNumber($request->request->get('credict_card_number'));

            //Asignamos la reserva al tipo de habitación y al usuario que la crea si corresponde
            $booking->setRoomType($room_type);
            $booking->setUser($this->getUser());
        
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($booking);
            $manager->flush();

            return $this->render('booking_success.html.twig', ['booking' => $booking]);
        }
        $this->addFlash(
            'error',
            'Error intentar reservar. Revise las fechas seleccionadas.'
        );

        return $this->redirect('/');
    }
    
}