<?php
namespace App\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\RoomType;

class RoomController extends AbstractController
{
    /**
    * @Route("/room/create", name="room-create", methods={"POST"})
    * @IsGranted("ROLE_ADMIN")
    * Método que crea un nuevo tipo de habitación
    */
    public function create(Request $request)
    {
        $from_date = new \DateTime($request->request->get('availability_from'));
        $to_date = new \DateTime($request->request->get('availability_to'));

        //Comprobamos que las fechas son correctas
        if($from_date > $to_date){
            $this->addFlash(
                'error',
                'Error al crear. Las fechas de disponibilidad no son correctas.'
            );
            return $this->redirectToRoute('dashboard', ['view' => 'rooms']);
        }

        $room_type = new RoomType();
        $room_type->setName($request->request->get('name'));
        $room_type->setAvailabilityFrom($from_date);
        $room_type->setAvailabilityTo($to_date);
        $room_type->setPricePerNight($request->request->get('price_per_night'));
        $room_type->setMaxCapacity($request->request->get('max_capacity'));
        $room_type->setRoomsQuantity($request->request->get('rooms_quantity'));
   
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($room_type);
        $manager->flush();

        $this->addFlash(
            'success',
            'Habitación creada con éxito.'
        );

        return $this->redirectToRoute('dashboard', ['view' => 'rooms']);
    }

    /**
    * @Route("/room/edit", name="room-edit", methods={"POST"})
    * @IsGranted("ROLE_ADMIN")
    * Método que actualiza un tipo de habitación
    */
    public function edit(Request $request)
    {
        $from_date = new \DateTime($request->request->get('availability_from'));
        $to_date = new \DateTime($request->request->get('availability_to'));
        $room_type = $this->getDoctrine()->getRepository(RoomType::class)->find($request->request->get('room_id'));
        //Comprobamos que existe el tipo de habitación a editar
        if (!$room_type) {
            throw $this->createNotFoundException('El tipo de habitación no existe');
        }
        //Comprobamos que las fechas son correctas
        if($from_date > $to_date){
            $this->addFlash(
                'error',
                'Error al actualizar. Las fechas de disponibilidad no son correctas.'
            );
            return $this->redirectToRoute('dashboard', ['view' => 'rooms']);
        }
        $manager = $this->getDoctrine()->getManager();

        $room_type->setName($request->request->get('name'));
        $room_type->setAvailabilityFrom($from_date);
        $room_type->setAvailabilityTo($to_date);
        $room_type->setPricePerNight($request->request->get('price_per_night'));
    
        $manager->flush();

        $this->addFlash(
            'success',
            'Habitación actualizada con éxito.'
        );

        return $this->redirectToRoute('dashboard', ['view' => 'rooms']);
    }
    
}