<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190225171044 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE booking (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, room_type_id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(60) NOT NULL, surname VARCHAR(60) NOT NULL, from_date DATETIME NOT NULL, to_date DATETIME NOT NULL, locator VARCHAR(255) NOT NULL, num_people INTEGER NOT NULL, total_cost DOUBLE PRECISION NOT NULL, phone VARCHAR(12) NOT NULL, credict_card_number VARCHAR(20) NOT NULL, extra_notes VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE296E3073 ON booking (room_type_id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDEA76ED395 ON booking (user_id)');
        $this->addSql('CREATE TABLE room_type (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(60) NOT NULL, availability_from DATETIME NOT NULL, availability_to DATETIME NOT NULL, price_per_night DOUBLE PRECISION NOT NULL, max_capacity INTEGER NOT NULL, rooms_quantity INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE app_users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(25) NOT NULL, email VARCHAR(254) NOT NULL, name VARCHAR(30) NOT NULL, surname VARCHAR(30) NOT NULL, password VARCHAR(64) NOT NULL, is_active BOOLEAN NOT NULL, roles CLOB NOT NULL --(DC2Type:array)
        )');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C2502824F85E0677 ON app_users (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C2502824E7927C74 ON app_users (email)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE room_type');
        $this->addSql('DROP TABLE app_users');
    }
}
