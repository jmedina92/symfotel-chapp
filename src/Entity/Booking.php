<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $surname;

    /**
     * @ORM\Column(type="datetime")
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $to_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $locator;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_people;

    /**
     * @ORM\Column(type="float")
     */
    private $total_cost;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $credict_card_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $extra_notes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RoomType", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookings")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getFromDate(): ?\DateTimeInterface
    {
        return $this->from_date;
    }

    public function setFromDate(\DateTimeInterface $from_date): self
    {
        $this->from_date = $from_date;

        return $this;
    }

    public function getToDate(): ?\DateTimeInterface
    {
        return $this->to_date;
    }

    public function setToDate(\DateTimeInterface $to_date): self
    {
        $this->to_date = $to_date;

        return $this;
    }

    public function getLocator(): ?string
    {
        return $this->locator;
    }

    public function setLocator(string $locator): self
    {
        $this->locator = $locator;

        return $this;
    }

    public function getNumPeople(): ?int
    {
        return $this->num_people;
    }

    public function setNumPeople(int $num_people): self
    {
        $this->num_people = $num_people;

        return $this;
    }

    public function getRoomTypeId(): ?int
    {
        return $this->room_type_id;
    }

    public function setRoomTypeId(int $room_type_id): self
    {
        $this->room_type_id = $room_type_id;

        return $this;
    }

    public function getTotalCost(): ?float
    {
        return $this->total_cost;
    }

    public function setTotalCost(float $total_cost): self
    {
        $this->total_cost = $total_cost;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCredictCardNumber(): ?string
    {
        return $this->credict_card_number;
    }

    public function setCredictCardNumber(string $credict_card_number): self
    {
        $this->credict_card_number = $credict_card_number;

        return $this;
    }

    public function getExtraNotes(): ?string
    {
        return $this->extra_notes;
    }

    public function setExtraNotes(?string $extra_notes): self
    {
        $this->extra_notes = $extra_notes;

        return $this;
    }

    public function getRoomType(): ?RoomType
    {
        return $this->room_type;
    }

    public function setRoomType(?RoomType $room_type): self
    {
        $this->room_type = $room_type;

        return $this;
    }

    public function getNumDays(): ?int
    {
        return date_diff($this->from_date,$this->to_date)->days + 1;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Array
     * Convierte en array el elemento
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'locator' => $this->locator,
            'num_people' => $this->num_people,
            'total_cost' => $this->total_cost,
            'total_cost_formated' => number_format($this->total_cost,2,',','.'),
            'from_date' => $this->from_date->format('Y-m-d'),
            'to_date' => $this->to_date->format('Y-m-d'),
            'phone' => $this->phone,
            'extra_notes' => $this->extra_notes,
            'room_type' => $this->room_type->toArray()
        ];
    }
}
