<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomTypeRepository")
 */
class RoomType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $availability_from;

    /**
     * @ORM\Column(type="datetime")
     */
    private $availability_to;

    /**
     * @ORM\Column(type="float")
     */
    private $price_per_night;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_capacity;

    /**
     * @ORM\Column(type="integer")
     */
    private $rooms_quantity;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="room_type")
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAvailabilityFrom(): ?\DateTimeInterface
    {
        return $this->availability_from;
    }

    public function setAvailabilityFrom(\DateTimeInterface $availability_from): self
    {
        $this->availability_from = $availability_from;

        return $this;
    }

    public function getAvailabilityTo(): ?\DateTimeInterface
    {
        return $this->availability_to;
    }

    public function setAvailabilityTo(\DateTimeInterface $availability_to): self
    {
        $this->availability_to = $availability_to;

        return $this;
    }

    public function getPricePerNight(): ?float
    {
        return $this->price_per_night;
    }

    public function setPricePerNight(float $price_per_night): self
    {
        $this->price_per_night = $price_per_night;

        return $this;
    }

    public function getMaxCapacity(): ?int
    {
        return $this->max_capacity;
    }

    public function setMaxCapacity(int $max_capacity): self
    {
        $this->max_capacity = $max_capacity;

        return $this;
    }

    public function getRoomsQuantity(): ?int
    {
        return $this->rooms_quantity;
    }

    public function setRoomsQuantity(int $rooms_quantity): self
    {
        $this->rooms_quantity = $rooms_quantity;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setRoomType($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getRoomType() === $this) {
                $booking->setRoomType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     * Obtiene las reservas para unas fechas solicitadas.
     * Excluye las reservas cuya entrada-salida se encuentra antes de la fecha 'desde' o después de la fecha 'hasta'.
     */

    public function getBookingsBetweenDates(\DateTimeInterface $from_date,\DateTimeInterface $to_date)
    {
        return $this->getBookings()->filter(function(Booking $booking) use ($from_date, $to_date) {
            return  !(($booking->getFromDate() < $from_date && $booking->getToDate() < $from_date) ||
                    ($booking->getFromDate() > $to_date && $booking->getToDate() > $to_date));
        });
    }

    /**
     * @return Integer
     * Obtiene el número de habitaciones disponibles.
     * Calculado en base a la diferencia entre el número de habitaciones totales y las reservadas para unas fechas dadas. 
     */

    public function getAvailabilityBetweenDates(\DateTimeInterface $from_date,\DateTimeInterface $to_date)
    {
        return $this->rooms_quantity - $this->getBookingsBetweenDates($from_date, $to_date)->count();
    }

    /**
     * @return Float
     * Calcula el precio para un número de noches dado
     */

    public function getPriceForTotalNights($num_days)
    {
        return $this->price_per_night * $num_days;
    }

    /**
     * @return Array
     * Convierte en array el elemento
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'max_capacity' => $this->max_capacity,
            'availability_from' => $this->availability_from->format('Y-m-d'),
            'availability_to' => $this->availability_to->format('Y-m-d'),
            'price_per_night' => $this->price_per_night,
            'bookings_count' => $this->getBookings()->count(),
        ];
    }
}
