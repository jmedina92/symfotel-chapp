<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\RoomType;
use App\Entity\Booking;

class RoomTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $start_date = new \DateTime('2019-01-01');
        $end_date = new \DateTime('2019-12-31');

        $room_type1 = new RoomType();
        $room_type1->setName('Individual');
        $room_type1->setAvailabilityFrom($start_date);
        $room_type1->setAvailabilityTo($end_date);
        $room_type1->setPricePerNight(20);
        $room_type1->setMaxCapacity(1);
        $room_type1->setRoomsQuantity(10);
        
        $room_type2 = new RoomType();
        $room_type2->setName('Doble');
        $room_type2->setAvailabilityFrom($start_date);
        $room_type2->setAvailabilityTo($end_date);
        $room_type2->setPricePerNight(30);
        $room_type2->setMaxCapacity(2);
        $room_type2->setRoomsQuantity(15);

        $room_type3 = new RoomType();
        $room_type3->setName('Triple');
        $room_type3->setAvailabilityFrom($start_date);
        $room_type3->setAvailabilityTo($end_date);
        $room_type3->setPricePerNight(40);
        $room_type3->setMaxCapacity(3);
        $room_type3->setRoomsQuantity(15);

        $room_type4 = new RoomType();
        $room_type4->setName('Cuádruple');
        $room_type4->setAvailabilityFrom($start_date);
        $room_type4->setAvailabilityTo($end_date);
        $room_type4->setPricePerNight(50);
        $room_type4->setMaxCapacity(4);
        $room_type4->setRoomsQuantity(10);
        
        /* $booking = new Booking();
        $booking->setEmail('aaaa');
        $booking->setName("test");
        $booking->setSurname("testsur");
        $booking->setFromDate($start_date);
        $booking->setToDate($end_date);
        $booking->setLocator('aaaa');
        $booking->setNumPeople(1);
        $booking->setTotalCost(11);
        $booking->setPhone('aaaa');
        $booking->setCredictCardNumber('aaaa');
        $booking->setRoomType($room_type2);
        $manager->persist($booking); */
        
        $manager->persist($room_type1);
        $manager->persist($room_type2);
        $manager->persist($room_type3);
        $manager->persist($room_type4);

        $manager->flush();
    }
}
